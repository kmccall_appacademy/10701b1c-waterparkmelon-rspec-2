def reverser(&prc)
  str = prc.call
  str.split.map{ |word| word.reverse}.join(" ")
end

def adder(value = 1, &prc)
  val = prc.call
  val += value
end

def repeater(time = 1, &prc)
  time.times do
    prc.call
  end
end
