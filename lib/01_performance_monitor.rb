def measure(value = nil, &prc)
  time1 = Time.now

  if value
    value.times do
      prc.call
    end
  else
    prc.call
  end

  time2 = Time.now

  if value
    (time2 - time1)/value
  else
    time2 - time1
  end
end
